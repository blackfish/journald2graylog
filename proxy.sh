#!/bin/sh
ncat -l -k -m "$MAX_CONNECTIONS" \
     --ssl --ssl-verify \
     --ssl-trustfile /certs."$(whoami)"/"$CACERT" \
     --ssl-cert /certs."$(whoami)"/"$CERT" \
     --ssl-key /certs."$(whoami)"/"$KEY" \
     "$LISTEN_PORT" | J2G_HOSTNAME="$GRAYLOG_HOSTNAME" J2G_PORT="$GRAYLOG_GELF_PORT" J2G_PACKET_SIZE="$GRAYLOG_GELF_PACKET_SIZE" /journald2graylog
